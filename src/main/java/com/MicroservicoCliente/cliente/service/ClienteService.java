package com.MicroservicoCliente.cliente.service;

import com.MicroservicoCliente.cliente.Consistencia.ClienteConsistencia;
import com.MicroservicoCliente.cliente.models.Cliente;
import com.MicroservicoCliente.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente) {
        try {
            return clienteRepository.save(cliente);
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao cadastrar o cliente: " +
                    e.getMessage(), e);
        }
    }


    public Cliente consultarClientePorId(long id) {

            Optional<Cliente> cliente = clienteRepository.findById(id);
            if (cliente.isPresent()) {
                return cliente.get();
            } else {
                throw new ClienteConsistencia();
                //throw new RuntimeException("Cliente não cadastrado - tratamento de exception.");
            }

    }
}
