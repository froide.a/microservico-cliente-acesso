package com.MicroservicoCliente.cliente;

import com.MicroservicoCliente.ClienteDecoder.ClienteDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteConfiguration {

    @Bean
    public ErrorDecoder getClienteDecoder() {
        return new ClienteDecoder();
    }


}
