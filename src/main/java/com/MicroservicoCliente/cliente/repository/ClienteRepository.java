package com.MicroservicoCliente.cliente.repository;

import com.MicroservicoCliente.cliente.models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
