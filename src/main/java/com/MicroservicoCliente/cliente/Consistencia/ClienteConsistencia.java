package com.MicroservicoCliente.cliente.Consistencia;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente não cadastrado - tratamento de exception.")
public class ClienteConsistencia extends RuntimeException {

}
