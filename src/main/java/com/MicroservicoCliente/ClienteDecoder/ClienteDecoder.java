package com.MicroservicoCliente.ClienteDecoder;

import com.MicroservicoCliente.cliente.Consistencia.ClienteConsistencia;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new ClienteConsistencia();
        }
        return errorDecoder.decode(s, response);
    }

}
